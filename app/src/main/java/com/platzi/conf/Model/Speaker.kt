package com.platzi.conf.Model

import java.io.Serializable

class Speaker: Serializable {
    var name = ""
    var jobtitle = ""
    var workplace = ""
    var biography = ""
    var twitter = ""
    var image = ""
    var category = 0
}