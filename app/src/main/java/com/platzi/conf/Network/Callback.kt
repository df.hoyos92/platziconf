package com.platzi.conf.Network

import com.platzi.conf.Model.Conference
import java.lang.Exception

interface Callback<T> {
    fun onSuccess(result: T?)
    fun onFailed(exception: Exception)
}