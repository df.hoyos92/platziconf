package com.platzi.conf.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.platzi.conf.Model.Speaker
import com.platzi.conf.Network.Callback
import com.platzi.conf.Network.FirestoreService
import java.lang.Exception

class SpeakersViewModel: ViewModel() {
    val firestoreService = FirestoreService()
    val listSpeaker: MutableLiveData<List<Speaker>> = MutableLiveData()
    var isLoading = MutableLiveData<Boolean>()

    fun refresh(){
        getSpeakerFromFirebase()
    }

    fun getSpeakerFromFirebase(){
        firestoreService.getSpeakers(object: Callback<List<Speaker>> {


            override fun onFailed(exception: Exception) {
                processFinish()
            }

            override fun onSuccess(result: List<Speaker>?) {
                listSpeaker.postValue(result)
                processFinish()
            }

        })
    }

    fun processFinish(){
        isLoading.value = true
    }
}