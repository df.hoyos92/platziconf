package com.platzi.conf.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.platzi.conf.Model.Conference
import com.platzi.conf.Network.Callback
import com.platzi.conf.Network.FirestoreService
import java.lang.Exception

class ScheduleViewModel: ViewModel() {
    val firestoreService = FirestoreService()
    val listSchedule: MutableLiveData<List<Conference>> = MutableLiveData()
    var isLoading = MutableLiveData<Boolean>()

    fun refresh(){
        getScheduleFromFirebase()
    }

    fun getScheduleFromFirebase(){
        firestoreService.getSchedule(object: Callback<List<Conference>> {


            override fun onFailed(exception: Exception) {
                processFinish()
            }

            override fun onSuccess(result: List<Conference>?) {
                listSchedule.postValue(result)
                processFinish()
            }

        })
    }

    fun processFinish(){
        isLoading.value = true
    }
}