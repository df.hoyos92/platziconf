package com.platzi.conf.View.Ui.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.platzi.conf.Model.Speaker
import com.platzi.conf.R
import com.platzi.conf.View.Adapters.SpeakerAdapter
import com.platzi.conf.View.Adapters.SpeakerListener
import com.platzi.conf.ViewModel.SpeakersViewModel
import com.platzi.conf.databinding.FragmentSpeakersBinding


class SpeakersFragment : Fragment(), SpeakerListener {

    private lateinit var fragmentSpeakerBinding: FragmentSpeakersBinding
    private lateinit var speakerAdapter: SpeakerAdapter
    private lateinit var viewModel: SpeakersViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentSpeakerBinding = FragmentSpeakersBinding.inflate(LayoutInflater.from(container?.context))
        return fragmentSpeakerBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this)[SpeakersViewModel::class.java]
        viewModel.refresh()

        speakerAdapter = SpeakerAdapter(this)
        fragmentSpeakerBinding.rvSpeakers.apply {
            layoutManager = GridLayoutManager(view.context,2)
            adapter = speakerAdapter
        }
        observeViewModel()
    }
    fun observeViewModel(){
        viewModel.listSpeaker.observe(viewLifecycleOwner, Observer<List<Speaker>> { speaker ->
            speakerAdapter.updateData(speaker)
        })
        viewModel.isLoading.observe(viewLifecycleOwner, Observer<Boolean>{
            if(it != null)
                fragmentSpeakerBinding.rlBase.visibility = View.INVISIBLE
        })
    }

    override fun onSpeakerClicked(speaker: Speaker, position: Int) {
        val bundle = bundleOf("speaker" to speaker)
        findNavController().navigate(R.id.speakerDetailFragmenDialog, bundle)
    }
}