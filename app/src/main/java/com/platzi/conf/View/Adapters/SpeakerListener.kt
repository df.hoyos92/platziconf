package com.platzi.conf.View.Adapters

import com.platzi.conf.Model.Speaker

interface SpeakerListener {
    fun onSpeakerClicked(speaker: Speaker, position: Int)
}