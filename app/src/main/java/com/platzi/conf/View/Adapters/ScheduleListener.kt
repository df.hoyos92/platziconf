package com.platzi.conf.View.Adapters

import com.platzi.conf.Model.Conference


interface ScheduleListener {
    fun onConferenceClicked(conference: Conference, position: Int)
}