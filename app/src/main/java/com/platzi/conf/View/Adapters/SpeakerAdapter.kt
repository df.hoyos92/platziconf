package com.platzi.conf.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.platzi.conf.Model.Speaker
import com.platzi.conf.R

class SpeakerAdapter(val speakerListerner: SpeakerListener)  : RecyclerView.Adapter<SpeakerAdapter.ViewHolder>() {


    var listSpeaker = ArrayList<Speaker>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SpeakerAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_speaker, parent, false))

    override fun onBindViewHolder(holder: SpeakerAdapter.ViewHolder, position: Int) {
        val speaker = listSpeaker[position]
        holder.tvSpeakerName.text = speaker.name
        holder.tvSpeakerWork.text = speaker.workplace

        Glide.with(holder.itemView.context)
            .load(speaker.image)
            .apply(RequestOptions.circleCropTransform())
            .error(R.drawable.ic_launcher_foreground)
            .into(holder.ivSpeakerImage)

        holder.itemView.setOnClickListener{
            speakerListerner.onSpeakerClicked(speaker, position)
        }
    }

    override fun getItemCount() = listSpeaker.size

    fun updateData(data: List<Speaker>){
        listSpeaker.clear()
        listSpeaker.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) :  RecyclerView.ViewHolder(itemView) {
        val tvSpeakerName = itemView.findViewById<TextView>(R.id.tvSpeakerName)
        val tvSpeakerWork = itemView.findViewById<TextView>(R.id.tvSpeakerWork)
        val ivSpeakerImage = itemView.findViewById<ImageView>(R.id.ivSpeakerImage)
    }
}